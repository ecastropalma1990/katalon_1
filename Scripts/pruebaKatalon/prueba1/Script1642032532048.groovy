import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://preclinico.rayenaps.cl/')

WebUI.setText(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/input_Ubicacin_location'), 'CesfamRayenSalud')

WebUI.setText(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/input_Usuario_username'), '169910782')

WebUI.setEncryptedText(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/input_Clave_password'), 'uea9bIQUA4g/nt3sSKXTeQ==')

WebUI.click(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/button_Ingresar'))

WebUI.verifyElementNotPresent(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/h5_Cargando'), 0)

WebUI.click(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/i_Rayen_fal fa-bars navbar-left-icon'))

WebUI.click(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/li_Box'))

WebUI.click(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/span_Pacientes citados'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/div_Selecione el lugar de atencin'), 
    0)

WebUI.selectOptionByValue(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/select_Pasillo Pediatria  Sector Amarillo S_f5c190'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/select_Box Pediatria'), '2357', true)

WebUI.click(findTestObject('Object Repository/TC_LoginExitozo/Page_Rayen/button_Aceptar'))

