import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.G_Url)

WebUI.setText(findTestObject('Object Repository/RAYEN_2020_CLINICO/LOGIN/TC_3638 - Inicio de sesión usuario válido/Page_Rayen/input_Ubicacin_location'), 
    GlobalVariable.G_Ubicacion)

WebUI.setText(findTestObject('Object Repository/RAYEN_2020_CLINICO/LOGIN/TC_3638 - Inicio de sesión usuario válido/Page_Rayen/input_Usuario_username'), 
    GlobalVariable.G_Usuario)

WebUI.setText(findTestObject('Object Repository/RAYEN_2020_CLINICO/LOGIN/TC_3638 - Inicio de sesión usuario válido/Page_Rayen/input_Clave_password'), 
    GlobalVariable.G_Clave)

WebUI.click(findTestObject('Object Repository/RAYEN_2020_CLINICO/LOGIN/TC_3638 - Inicio de sesión usuario válido/Page_Rayen/button_Ingresar'))

WebUI.verifyElementNotPresent(findTestObject('Object Repository/RAYEN_2020_CLINICO/LOGIN/TC_3638 - Inicio de sesión usuario válido/Page_Rayen/button_Ingresar'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.verifyElementNotPresent(findTestObject('TC_LoginExitozo/Page_Rayen/h5_Cargando'), GlobalVariable.G_LongTimeDelay)

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/LOGIN/TC_3638 - Inicio de sesión usuario válido/Page_Rayen/a_Rayen'), 
    GlobalVariable.G_LongTimeDelay)

