import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/div_Atencin actual'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/div_Atencin actual'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/a_3'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/a_3'))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/span_Cancelar atencin'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/h5_Cancelar atencin'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/h5_Cancelar atencin'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/div_Cancelar atencin'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/div_Cancelar atencin'))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/button_Si'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/a_Espontneo'), 
    GlobalVariable.G_LongTimeDelay)

