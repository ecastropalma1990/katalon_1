import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/a_Espontneo'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/a_Espontneo'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/input_PasaporteOtros_documentNumber'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.setText(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/input_PasaporteOtros_documentNumber'), 
    '93.738.668-0')

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/button_Buscar'))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/div_937386680'))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/button_Aceptar_Busqueda_Local_Usuarios'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/h5_Datos Atencin Espontnea'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.selectOptionByValue(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/select_Seleccione instrumentoEnfermero(a)Ki_88103c'), 
    '7', true)

WebUI.setText(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/input_Tipo de Atencin_attentionType'), 
    'morb')

WebUI.sendKeys(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/input_Tipo de Atencin_attentionType'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/li_Otras Morbilidades'))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/button_Aceptar_AtencionEspontanea'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/ATENCION_ESPONTANEA/TC_3105 - Crear Atención Espontanea(PacienteInscrito)/Page_Rayen/div_Atencin actual'), 
    GlobalVariable.G_LongTimeDelay)

