import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('RAYEN_2020_CLINICO/LOGIN/TC_3638 - Inicio de sesión usuario válido'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/i_Rayen_Menu'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/i_Rayen_Menu'))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/span_Box'))

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/span_Pacientes citados'))

WebUI.verifyElementPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/div_Selecione_Lugar_Atencion'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.verifyElementNotPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/Page_Rayen/select_Cargando_ubicacion'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.selectOptionByValue(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/select_Ubicacion_Lugar_Atencion'), 
    '2', true)

WebUI.verifyElementNotPresent(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/Page_Rayen/select_Cargando_box'), 
    GlobalVariable.G_LongTimeDelay)

WebUI.selectOptionByValue(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/select_Box_Lugar_Atencion'), 
    '2357', true)

WebUI.click(findTestObject('RAYEN_2020_CLINICO/BOX/PACIENTES_CITADOS/LUGAR_ATENCION/TC_000 - Lugar de Atencion/Page_Rayen/button_Aceptar_Lugar_Atencion'))

